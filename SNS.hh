<?hh //partial
namespace nuclio\plugin\messaging\sns
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;

	use Aws\Sns\SnsClient;
	use Aws\Sns\Exception;

	<<factory>>
	class SNS extends Plugin
	{
		private SnsClient $client;

		public static function getInstance(/* HH_FIXME[4033] */...$args):SNS
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		/**
		 * Creates the Amazon SNS client using the given credentials
		 * @param string $awsKey       The amazon key
		 * @param string $awsSecretKey The amazon secret
		 * @param string $region       region for the end point of the Amazon SNS API. valid values are:
		 *                             	REGION:					 |  VALUE:			| URL:								|	PROTOCOLS:
		 *								US East (N. Virginia)	 |	us-east-1		| sns.us-east-1.amazonaws.com		|	HTTP and HTTPS
		 *								US West (Oregon)		 |	us-west-2		| sns.us-west-2.amazonaws.com		|	HTTP and HTTPS
		 *								US West (N. California)	 |	us-west-1		| sns.us-west-1.amazonaws.com		|	HTTP and HTTPS
		 *								EU (Ireland)			 |	eu-west-1		| sns.eu-west-1.amazonaws.com		|	HTTP and HTTPS
		 *								EU (Frankfurt)			 |	eu-central-1	| sns.eu-central-1.amazonaws.com	|	HTTP and HTTPS
		 *								Asia Pacific (Singapore) |	ap-southeast-1	| sns.ap-southeast-1.amazonaws.com	|	HTTP and HTTPS
		 *								Asia Pacific (Sydney)	 |	ap-southeast-2	| sns.ap-southeast-2.amazonaws.com	|	HTTP and HTTPS
		 *								Asia Pacific (Tokyo)	 |	ap-northeast-1	| sns.ap-northeast-1.amazonaws.com	|	HTTP and HTTPS
		 *								South America (Sao Paulo)|	sa-east-1		| sns.sa-east-1.amazonaws.com		|	HTTP and HTTPS
		 */
		public function __construct(string $awsKey, string $awsSecretKey, string $region):void
		{
			$this->client = SnsClient::factory
			(
				[
					'credentials' =>
					[
					    'key'      => $awsKey,
					    'secret'   => $awsSecretKey
				    ],
				    'region' => $region
				]
			);
			parent::__construct();
		}
		/**
		 * Creates a new topic with the given name
		 * @param  string $name topic name
		 * @return string       the topic arn
		 */
		public function createTopic(string $name):string 
		{
			$result = $this->client->createTopic(['Name'=>$name]);
			if(isset($result['TopicArn']))
			{
				return $result['TopicArn'];
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
				
			}
		}
		/**
		 * Deletes a topic
		 * @param  string 	$arn the topic arn
		 * @return bool     true on success, exception on failure
		 */
		public function deleteTopic(string $arn):bool 
		{
			try
			{
				$result = $this->client->deleteTopic(['TopicArn'=>$arn]);	
			}
			catch(Exception $e)
			{
				throw new SNSException($e->getMessage());
			}
			return true;
		}
		/**
		 * Returns a list of topics limited to a 100. If the number of topics is bigger than a 100 a Next token is returned as well. 
		 * Call the method again with the token as parameterto get the following topics.
		 * @param  string OPTIONAL the next token to get the next set of a 100 topics if the list of topic il longer than a 100.
		 * @return  Vector List of topic arns
		 */
		public function listTopics(?string $next=null):Vector<string>
		{
			is_null($next) ? $result = $this->client->listTopics() : $result = $this->client->listTopics(['NextToken' => $next]);
			if(isset($result['Topics']))
			{
				$return =  Vector{};
				foreach ($result['Topics'] as $topic)
				{
					$return->add($topic['TopicArn']);
				}
				return $return;
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * Returns the atributes for the given topic
		 * @param  string $arn the topic arn
		 * @return Map      the topic attributes
		 */
		public function getTopicAttributes(string $arn):Map<string,string> 
		{
			$result = $this->client->GetTopicAttributes(['TopicArn'=>$arn]);
			if(isset($result['Attributes']))
			{
				return new Map($result['Attributes']);
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
				
			}
		}
		/**
		 * Sets the value for a single topic attribute
		 * @link http://docs.aws.amazon.com/sns/latest/APIReference/API_SetTopicAttributes.html
		 * @param string $arn   the topic arn
		 * @param string $name  the attribute name. NOTE: The valid name values are restricted to the preset topic attributes. See link for more informations
		 * @param string $value the attribute value
		 * @return bool 		true on success, exception on failure
		 */
		public function setTopicAttribute(string $arn, string $name, string $value):bool 
		{
			try
			{
				$result = $this->client->setTopicAttributes
				(
					[
						'TopicArn' => $arn,
						'AttributeName' => $name,
						'AttributeValue' => $value
					]
				);	
			}
			catch(Exception $e)
			{
				throw new SNSException($e->getMessage());
			}
			return true;
		}
		/**
		 * Subscribes an endpoint to a topic (All messages published on that topic will be sent to that endpoint). 
		 * The endpoint itself it's an optional parameter but the documentation does not specify what happens to a subscription with no endpoint.
		 *
		 * @link http://docs.aws.amazon.com/sns/latest/api/API_Subscribe.html
		 * @param string $TopicArn   the topic arn
	     * @param string $protocol   http/https/email/email-json/sms/sqs/application/lambda
	     * @param string $endpoint   depending on the protocol the endpoint must be an url, an email address, a phone number, an amazon arn
		 * @return string 			 the subscription arn NOTE: if the subscription is not yet confirmed the arn value will be: 'Pending Confirmation'. 
		 *                        	 Using this value in a subscription query will result in an exception.
		 */
		public function subscribe(string $arn, string $protocol, string $endpoint):string
		{
			$result = $this->client->subscribe
			(
				[
					'TopicArn' => $arn,
					'Protocol' => $protocol,
					'Endpoint' => $endpoint
				]
			);
			if(isset($result['SubscriptionArn']))
			{
				return $result['SubscriptionArn'];
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * Deletes a subscription. If the subscription requires authentication for deletion, only the owner of the subscription or the topic's owner can unsubscribe, 
		 * and an AWS signature is required. If the Unsubscribe call does not require authentication and the requester is not the subscription owner, 
		 * a final cancellation message is delivered to the endpoint, 
		 * so that the endpoint owner can easily resubscribe to the topic if the Unsubscribe request was unintended.
		 * @param  string $arn 		the subscription arn
		 * @return bool  			true on success, exception on failure
		 */
		public function unsubscribe(string $arn):bool
		{
			try
			{
				$result = $this->client->unsubscribe
				(
					[
						'SubscriptionArn' => $arn,
					]
				);	
			}
			catch(Exception $e)
			{
				throw new SNSException($e->getMessage());
				
			}
			return true;	
		}
		/**
		 * Verifies an endpoint owner's intent to receive messages by validating the token sent to the endpoint by an earlier Subscribe action. 
		 * If the token is valid, the action creates a new subscription and returns its Amazon Resource Name (ARN). 
		 * This call requires an AWS signature only when the AuthenticateOnUnsubscribe flag is set to "true".
		 * @param  string      $arn                     the topic arn
		 * @param  string      $token                   the confirmation token sent to the endpoint with the subscription request
		 * @param  string|null $authenticateOnSubscribe OPTIONAL 
		 * @return string                               the subscription arn
		 */
		public function confirmSubscription(string $arn, string $token, ?string $authenticateOnSubscribe=null):string
		{
			$params = 
			[
				'TopicArn' => $arn,
				'Token' => $token
			];
			is_null($authenticateOnSubscribe) ? null : $params['AuthenticateOnSubscribe'] = $authenticateOnSubscribe;
			$result = $this->client->confirmSubscription($params);
			if(isset($result['SubscriptionArn']))
			{
				return $result['SubscriptionArn'];
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
				
			}
		}
		/**
		 * Returns a list of all the subscriptions for the current SNS account
		 * @param  string|null $next OPTIONAL the next token to get the next batch of a 100 subscription.
		 * @return Vector            List of the subscription with structure:
		 * 
		 *						    SubscriptionArn => (string)
		 *						    Owner => (string)
		 *						    Protocol => (string)
		 *						    Endpoint => (string)
		 *						    TopicArn => (string)
		 */
		public function listSubscriptions(?string $next=null):Vector<Map>
		{
			is_null($next) ? $result = $this->client->listSubscriptions() : $result = $this->client->listSubscriptions(['NextToken' => $next]);
			if(isset($result['Subscriptions']))
			{
				$return = Vector{};
				foreach ($result['Subscriptions'] as $subscription)
				{
					$return->add(new Map($subscription));
				}
				return $return;
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * List of all the subscriptions fot the given topic
		 * @param  string      $arn  the topic arn
		 * @param  string|null $next OPTIONAL the next token to get the next batch of a 100 subscription.
		 * @return Vector            List of the subscription with structure:
		 * 
		 *						     SubscriptionArn => (string)
		 *						     Owner => (string)
		 *						     Protocol => (string)
		 *						     Endpoint => (string)
		 *						     TopicArn => (string)
		 */
		public function listSubscriptionsByTopic(string $arn, ?string $next=null):Vector<Map>
		{
			is_null($next) ? $result = $this->client->listSubscriptionsByTopic(['TopicArn'=>$arn]) : $result = $this->client->listSubscriptionsByTopic(['TopicArn' => $arn, 'NextToken' => $next]);
			if(isset($result['Subscriptions']))
			{
				$return = Vector{};
				foreach ($result['Subscriptions'] as $subscription)
				{
					$return->add(new Map($subscription));
				}
				return $return;
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * Sends a message to all of a topic's subscribed endpoints. 
		 * When a messageId is returned, the message has been saved and Amazon SNS will attempt to deliver it to the topic's subscribers shortly. 
		 * The format of the outgoing message to each subscribed endpoint depends on the notification protocol selected.
		 * @param  Map $params The message parameters:
		 * 
		 *					    'TopicArn' => 'string',
		 *					    'TargetArn' => 'string',
		 *					    // Message is required
		 *					    'Message' => 'string',
		 *					    'Subject' => 'string',
		 *					    'MessageStructure' => 'string',
		 *					    'MessageAttributes' => Map
		 *					    {
		 *					        // Associative array of custom 'String' key names
		 *					        'String' => Map
		 *					        {
		 *					           // DataType is required
		 *					            'DataType' => 'string',
		 *					            'StringValue' => 'string',
		 *					            'BinaryValue' => 'string',
		 *					        },
		 *				        // ... repeated
		 *					    },
		 *
		 * @return string      the message id
		 */
		public function publish(Map<string,mixed> $params):string
		{
			if(!$params->containsKey('TopicArn'))
			{
				throw new SNSException("The TopicArn is required");
			}
			if(!$params->containsKey('Message'))
			{
				throw new SNSException("The Message is required");
			}
			if($params->containsKey('MessageAttributes') && !is_null($attributes = $params->get('MessageAttributes')))
			{
				foreach ($attributes as $attribute)
				{
					if(!$attribute->containsKey('DataType'))
					{
						throw new SNSException("The DataType is required for every attribute");
					}
				}
				$params->set('MessageAttributes', $attributes->toArray());	
			}
			$result = $this->client->publish($params->toArray());
			if(isset($result['MessageId']))
			{
				return $result['MessageId'];
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * Creates a platform application object for one of the supported push notification services, such as APNS and GCM, to which devices and mobile apps may register. 
		 * You must specify PlatformPrincipal and PlatformCredential attributes when using the CreatePlatformApplication action. 
		 * The PlatformPrincipal is received from the notification service. For APNS/APNS_SANDBOX, PlatformPrincipal is "SSL certificate". 
		 * For GCM, PlatformPrincipal is not applicable. For ADM, PlatformPrincipal is "client id". The PlatformCredential is also received from the notification service. 
		 * For WNS, PlatformPrincipal is "Package Security Identifier". For MPNS, PlatformPrincipal is "TLS certificate". For Baidu, PlatformPrincipal is "API key".
         * For APNS/APNS_SANDBOX, PlatformCredential is "private key". For GCM, PlatformCredential is "API key". For ADM, PlatformCredential is "client secret". 
         * For WNS, PlatformCredential is "secret key". For MPNS, PlatformCredential is "private key". For Baidu, PlatformCredential is "secret key". 
         * The PlatformApplicationArn that is returned when using CreatePlatformApplication is then used as an attribute for the CreatePlatformEndpoint action.
         * @link http://docs.aws.amazon.com/sns/latest/api/API_CreatePlatformApplication.html
         * @param   $name Application name - Application names must be made up of only uppercase and lowercase ASCII letters, numbers, underscores, hyphens, and periods, and must be between 1 and 256 characters long.
         * @param   $platform - The following platforms are supported: ADM (Amazon Device Messaging), APNS (Apple Push Notification Service), APNS_SANDBOX, and GCM (Google Cloud Messaging).
         * @param   $attributes - A list of the platform attributes: (see link for more informations)
         *    PlatformCredential -- The credential received from the notification service. For APNS/APNS_SANDBOX, PlatformCredential is private key. For GCM, PlatformCredential is "API key". For ADM, PlatformCredential is "client secret".
		 *    PlatformPrincipal -- The principal received from the notification service. For APNS/APNS_SANDBOX, PlatformPrincipal is SSL certificate. For GCM, PlatformPrincipal is not applicable. For ADM, PlatformPrincipal is "client id".
		 *    EventEndpointCreated -- Topic ARN to which EndpointCreated event notifications should be sent.
		 *    EventEndpointDeleted -- Topic ARN to which EndpointDeleted event notifications should be sent.
		 *    EventEndpointUpdated -- Topic ARN to which EndpointUpdate event notifications should be sent.
		 *    EventDeliveryFailure -- Topic ARN to which DeliveryFailure event notifications should be sent upon Direct Publish delivery failure (permanent) to one of the application's endpoints.
		 *    SuccessFeedbackRoleArn -- IAM role ARN used to give Amazon SNS write access to use CloudWatch Logs on your behalf.
		 *    FailureFeedbackRoleArn -- IAM role ARN used to give Amazon SNS write access to use CloudWatch Logs on your behalf.
		 *    SuccessFeedbackSampleRate -- Sample rate percentage (0-100) of successfully delivered messages.
		 * @return string the platform application ARM
		 */
		public function createPlatformApplication(string $name, string $platform, ?Map<string,string> $attributes=null):string
		{
			$params = 
			[
				'Name' => $name,
				'Platform' => $platform
			];
			if(!is_null($attributes))
			{
				$params['Attributes'] = $attributes->toArray();
			}
			$result = $this->client->createPlatformApplication($params);
			if(isset($result['PlatformApplicationArn']))
			{
				return $result['PlatformApplicationArn'];
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * Returns a list aof all the platform applications for the current SNS account
		 * @param  string|null $next OPTIONAL the next token to get the next batch of a 100 subscription.
		 * @return Vector            A list of the platform applications with structrure:
		 * 
		 *                          PlatformApplicationArn => (string)
		 *							Attributes => Map
		 *							{
		 *							    key => (string),
		 *							    value => (string)
		 *							}
		 */
		public function listPlatformApplications(?string $next=null):Vector<Map>
		{
			is_null($next) ? $result = $this->client->listPlatformApplications() : $result = $this->client->listPlatformApplications(['NextToken' => $next]);
			if(isset($result['PlatformApplications']))
			{
				$return = Vector{};
				foreach ($result['PlatformApplications'] as $application)
				{
					$attributes = Vector{};
					foreach ($application['Attributes'] as $attribute)
					{
						$attributes->add(new Map($attribute));
					}
					$return->add( Map {'PlatformApplicationArn'=>$application['PlatformApplicationArn'], 'Attributes'=>$attributes } );
				}
				return $return;
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * Sets the attributes of the given platform application
		 * @param  string $arn     	platform application ARN
		 * @param  Map $attributes   list of custom attribute=>value strings
		 * @return bool 			true on success, exception on failure
		 */
		public function setPlatformApplicationAttributes(string $arn, Map<string,string> $attributes):bool
		{
			try
			{
				$result = $this->client->setPlatformApplicationAttributes(['PlatformApplicationArn'=>$arn, 'Attributes'=>$attributes->toArray()]);	
			}
			catch(Exception $e)
			{
				throw new SNSException($e->getMessage());
			}
			return true;
		}
		/**
		 * Creates an endpoint for a device and mobile app on one of the supported push notification services, such as GCM and APNS. 
		 * CreatePlatformEndpoint requires the PlatformApplicationArn that is returned from CreatePlatformApplication. 
		 * The EndpointArn that is returned when using CreatePlatformEndpoint can then be used by the Publish action to send a message to a mobile app or by the Subscribe action for subscription to a topic. 
		 * The CreatePlatformEndpoint action is idempotent, so if the requester already owns an endpoint with the same device token and attributes, that endpoint's ARN is returned without creating a new endpoint.
		 * @param  string $arn              the platform application arn
		 * @param  string $token            Unique identifier created by the notification service for an app on a device. 
		 *                                  The specific name for Token will vary, depending on which notification service is being used. 
		 *                                  For example, when using APNS as the notification service, you need the device token. 
		 *                                  Alternatively, when using GCM or ADM, the device token equivalent is called the registration ID.
		 * @param  string $customerUserData OPTIONAL Arbitrary user data to associate with the endpoint. Amazon SNS does not use this data. The data must be in UTF-8 format and less than 2KB.
		 * @param  Map 	  $attributes       List of the endpoint attributes. For more information see: http://docs.aws.amazon.com/sns/latest/api/API_SetEndpointAttributes.html
		 * @return string                   the Endpoint Arn
		 */
		public function createPlatformEndpoint(string $arn, string $token, ?string $customerUserData, ?Map<string,string> $attributes):string
		{
			$params = 
			[
				'PlatformApplicationArn' =>$arn,
				'Token' => $token
			];
			is_null($customerUserData) ? null : $params['CustomerUserData'] = $customerUserData;
			if(!is_null($attributes))
			{
				$params['Attributes'] = $attributes->toArray();
			}
			$result = $this->client->createPlatformEndpoint($params);
			if(isset($result['EndpointArn']))
			{
				return $result['EndpointArn'];
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * Returns a list of the endpoints for the given platform application
		 * @param  string      $arn  the application platform arn
		 * @param  string|null $next OPTIONAL the next token to get the next batch of a 100 subscription.
		 * @return Vector            A list of endpoints wit structure:
		 * 
		 *						     EndpointArn => (string)
		 *						     Attributes => Map
		 *						     {
		 *				     	         key => (string)
		 *					             value => (string)
		 *						     }
		 */
		public function listEndpointsByPlatformApplication(string $arn, ?string $next=null):Vector<Map>
		{
			is_null($next) ? $result = $this->client->listEndpointsByPlatformApplication([' PlatformApplicationArn'=>$arn]) : $result = $this->client->listEndpointsByPlatformApplication([' PlatformApplicationArn' => $arn, 'NextToken' => $next]);
			if(isset($result['Endpoints']))
			{
				$return = Vector{};
				foreach ($result['Endpoints'] as $endpoint)
				{
					$attributes = Vector{};
					foreach ($endpoint['Attributes'] as $attribute)
					{
						$attributes->add(new Map($attribute));
					}
					$return->add( Map {'PlatformApplicationArn'=>$endpoint['EndpointArn'], 'Attributes'=>$attributes } );
				}
				return $return;
			}
			else
			{
				throw new SNSException("Something went wrong. Please try again.");
			}
		}
		/**
		 * Sets the attributes of the given endpoint
		 * @param string $arn     Endpoint ARN
		 * @param Map $attributes   list of custom attribute=>value strings
		 * @return bool 			true on success, exception on failure
		 */
		public function setEndpointAttributes(string $arn, Map<string,string> $attributes):bool
		{
			try
			{
				$result = $this->client->setEndpointAttributes(['EndpointArn'=>$arn, 'Attributes'=>$attributes->toArray()]);	
			}
			catch(Exception $e)
			{
				throw new SNSException($e->getMessage());
			}
			return true;
		}
	}
}